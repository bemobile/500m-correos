# -*- coding:utf-8 -*-
from base import BaseModel


class Shipment(BaseModel):
    def __init__(self, pickup_datetime=None, number_of_packages=None, weight=None, value=None, observations=None,
                 receiver=None, sender=None, declared_value=None, reference=None, locator=None, history=None, fee=None,
                 status=None):
        self.pickup_datetime = pickup_datetime
        self.number_of_packages = number_of_packages
        self.weight = weight
        self._default_weight = 1100
        self.value = value
        self.observations = observations
        self.receiver = receiver
        self.sender = sender
        self.declared_value = declared_value
        self.reference = reference
        self.locator = locator
        self.history = history
        self.fee = fee
        self.status = status
        super(Shipment, self).__init__()

    def to_dict(self):
        d = {
            "CodProducto": "S0132",
            "ReferenciaCliente": self.reference,
            "TipoFranqueo": "FP",
            "ModalidadEntrega": "ST",
            "Pesos": {
                "Peso": {
                    "Valor": self._default_weight,
                    "TipoPeso": "R"
                }
            },
            "Observaciones1": self.observations

        }
        if self.value != 0:
            d.update({
                "ValoresAnadidos": {
                    "ImporteSeguro": str(int(self.value*100))
                }
            })
        return d

    def _build_data(self):
        from . import Config
        destinatario = self.receiver.to_dict()
        destinatario['DatosDireccion2'] = Address().to_dict()
        return {
            "FechaOperacion": self.pickup_datetime.strftime("%d-%m-%YT%H:%M:%S"),
            "CodEtiquetador": Config.auth['CodEtiquetador'],
            "Care": "000000",
            "TotalBultos": self.number_of_packages,
            "ModDevEtiqueta": "1",
            "Remitente": self.sender.to_dict(),
            "Destinatario": destinatario,
            "Envio": self.to_dict()
        }

    def save(self):
        data = self._build_data()
        result = self.insert(data)
        for key, value in result.items():
            setattr(self, key, value)
        return result

    def get(self, locator_number):
        result = self.select(locator_number)
        self.history = result.get('status_history', [])
        self.status = self.parse_status(result.get('status_history', []))
        self.locator = locator_number
        return self

    @staticmethod
    def parse_status(status_history):
        if filter(lambda x: 'entregado' in x.lower(), status_history):
            return 'Delivered'
        if filter(lambda x: 'admitido' in x.lower() or u'salida de envío de oficina de cambio destino' in x.lower() or \
                            u'en tránsito' in x.lower() or 'intento de entrega' in x.lower() or \
                            'en proceso de entrega' in x.lower(), status_history):
            return 'Picked'
        if filter(lambda x: 'pre registro' in x.lower(), status_history):
            return 'Created'
        return None


class Person(object):
    def __init__(self, name=None, last_name=None, phone_prefix=None, phone_number=None, email=None,
                 language=None, address=None):
        self.name = name
        self.last_name = last_name
        self.phone_prefix = phone_prefix
        self.phone_number = phone_number
        self.email = email
        self.language = language
        self.address = address

    def to_dict(self):
        return {
            "Identificacion": {
                "Nombre": self.name,
                "Apellido1": self.last_name
            },
            "DatosDireccion": self.address.to_dict(),
            "CP": self.address.postal_code,
            "Telefonocontacto": self.phone_number,
            "Email": self.email
        }


class Address(object):
    def __init__(self, street="", number="", floor="", door="", postal_code="", city="", province="", country=""):
        self.street = street
        self.number = number
        self.floor = floor
        self.door = door
        self.postal_code = postal_code
        self.city = city
        self.province = province
        self.country = country

    def to_dict(self):
        return {
            "Direccion": self.street,
            "Numero": self.number,
            "Piso": self.floor,
            "Puerta": self.door,
            "Localidad": self.city,
            "Provincia": self.province
        }
