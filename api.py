# -*- coding:utf-8 -*-
import requests
import zeep
from zeep.transports import Transport
from exceptions import APIError, DecodeError
import xmltodict


class WsdlBaseAPI(object):
    def __init__(self, specification, auth=None):
        from . import Config
        session = requests.Session()
        session.verify = False
        if auth:
            session.auth = auth
        self.client = zeep.Client(wsdl=self._absolute_url(specification), transport=Transport(session=session))
        self.debug = Config.debug
        self._set_logging()

    @property
    def type_factory(self):
        return self.client.type_factory('ns0')

    def _create_apierror(self, result, method=None, headers=None):
        error = result.BultoError.DescError
        raise APIError(error, content=result, headers=headers, url=method)

    def _create_decodeerror(self, result, method=None, error=None):
        raise DecodeError(result, content=error, url=method)

    @property
    def ns(self):
        return self.client.wsdl.types.get_ns_prefix('ns0')

    def _set_logging(self):
        if not self.debug:
            return

        import logging.config
        logging.config.dictConfig({
            'version': 1,
            'formatters': {
                'verbose': {
                    'format': '%(name)s: %(message)s'
                }
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'verbose',
                },
            },
            'loggers': {
                'zeep.transports': {
                    'level': 'DEBUG',
                    'propagate': True,
                    'handlers': ['console'],
                },
            }
        })


class WsdlInsertAPI(WsdlBaseAPI):
    def __init__(self, specification='', auth=None):
        from . import Config
        self.url = Config.hosts['INSERT']
        self.auth = auth
        super(WsdlInsertAPI, self).__init__(specification, auth)

    def _absolute_url(self, url):
        from . import Config
        pattern = '%s%s?wsdl'
        return pattern % (Config.hosts['BASE'], url)

    def _get_insert_service(self):
        return self.client.create_service(self.client.wsdl.bindings.keys()[0], self.url)

    def _get_headers(self):
        return {
            "Content-type": "application/soap+xml; charset=UTF-8"
        }

    def request(self, data):
        result = self._get_insert_service().PreRegistro(**data)
        if self.debug:
            print result
        try:
            if result.Resultado != 0:
                self._create_apierror(result, method='PreRegistro')
            return result
        except AttributeError as e:
            self._create_decodeerror(result, method='PreRegistro', error=e.message)

    def _parse_response(self, response):
            return {
                'reference': response.CodExpedicion,
                'status': 'Created',
                'fee': 0,
                'locator': response.Bulto.CodEnvio,
            }


class WsdlSelectAPI(WsdlBaseAPI):
    def __init__(self, specification='', auth=None):
        super(WsdlSelectAPI, self).__init__(specification, auth)

    def _absolute_url(self, url):
        from . import Config
        pattern = '%s%s.wsdl'
        return pattern % (Config.hosts['SELECT'], url)

    def _build_select_string(self, locator_number):
        if not locator_number:
            return None
        return """<?xml version="1.0" encoding="UTF-8"?>
        <ConsultaXMLin xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchemainstance" Idioma="1">
        <Consulta>
        <Codigo>%s</Codigo>
        </Consulta>
        </ConsultaXMLin>
        """ % (locator_number)

    def request(self, locator_number):
        select_string = self._build_select_string(locator_number)
        result = self.client.service.ConsultaLocalizacionEnviosFases(XMLin=select_string)
        if self.debug:
            print result
        return result

    def _parse_response(self, response):
        if response:
            xml_resp = xmltodict.parse(response.lower())
            history = None
            try:
                history = xml_resp.get('consultaxmlout', {}).get('respuestas', {}).get('datosidiomas', {})
            except Exception:
                self._create_decodeerror(response, method='ConsultaLocalizacionEnviosFases',
                                         error="No status history found.")

            if history and isinstance(history, list):
                history = history[0].get('datosenvios', {}).get('datos', {})
                status_history = []
                for status in history:
                    if status.get('estado', None):
                        status_history.append(status.get('estado'))
                return {
                    "status_history": status_history
                }
            elif history and not isinstance(history, list):
                history = history.get('datosenvios', {}).get('datos', {})
                if history and isinstance(history, list):
                    status_history = []
                    for status in history:
                        if status.get('estado', None):
                            status_history.append(status.get('estado'))
                    return {
                        "status_history": status_history
                    }
                return {
                    "status_history": [history.get('estado', None)]
                }
            else:
                self._create_decodeerror(response, method='ConsultaLocalizacionEnviosFases',
                                         error="No status history found.")
