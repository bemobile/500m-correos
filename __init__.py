class Config:
    auth = None
    debug = False
    hosts = None


class Correos(object):
    def __init__(self):
        self.config = Config

    def init_app(self, app):
        self.config.auth = app.config['CORREOS_SETTINGS']['AUTH']
        self.config.hosts = app.config['CORREOS_SETTINGS']['HOST']
        self.config.debug = app.config['DEBUG']
