import xml.etree.ElementTree as ET


def string_to_xml(string):
    return ET.fromstring(string)
