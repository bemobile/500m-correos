from api import WsdlInsertAPI, WsdlSelectAPI
from requests.auth import HTTPBasicAuth


class BaseModel(object):
    def __init__(self):
        from . import Config
        self.auth = HTTPBasicAuth(Config.auth['UserName'], Config.auth['Password'])

    def insert(self, data):
        handler = WsdlInsertAPI(auth=self.auth)
        response = handler.request(data)
        return handler._parse_response(response)

    def select(self, locator_number):
        handler = WsdlSelectAPI()
        response = handler.request(locator_number)
        return handler._parse_response(response)
